﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Entities
{
    public interface IEntity
    {
        [Key]
        int Id { set; get; }
        string Title { set; get; }
        DateTime CreateDate { set; get; }
        DateTime? UpdateDate { set; get; }
        string CreateBy { set; get; }
        string UpdateBy { set; get; }
    }
}
