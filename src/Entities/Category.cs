﻿using System;
using System.Collections.Generic;

namespace Entities
{
    public class Category : IEntity, IEntitySelfJoin<Category>
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string CreateBy { get; set; }
        public string UpdateBy { get; set; }
        public int? PId { set; get; }
        public string Description { set; get; }
        public int Level { set; get; }
        public virtual ICollection<Category> Children { set; get; }
        public virtual Category Father { set; get; }
        public virtual ICollection<Post> Posts { set; get; }
    }
}
