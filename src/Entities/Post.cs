﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities
{
    public class Post : IEntity
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string CreateBy { get; set; }
        public string UpdateBy { get; set; }
        public string Description { set; get; }
        public string Content { set; get; }
        public string Keyword { set; get; }
        public int CategoryId { set; get; }
        public virtual Category Category { set; get; }
    }
}
