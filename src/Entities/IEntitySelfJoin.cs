﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities
{
    public interface IEntitySelfJoin<T> where T:IEntity
    {
        int? PId { set; get; }
        int Level { set; get; }
        ICollection<T> Children { set; get; }
        T Father { set; get; }
    }
}
