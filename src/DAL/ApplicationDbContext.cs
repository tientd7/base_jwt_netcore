﻿using System;
using System.Collections.Generic;
using System.Text;
using Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace DAL
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public DbSet<Category> Categories { set; get; }
        public DbSet<Post> Posts { set; get; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Category>().HasKey(t => t.Id);
            builder.Entity<Post>().HasKey(t => t.Id);
            builder.Entity<Category>().HasMany(t => t.Children).WithOne(t => t.Father).HasForeignKey(t => t.PId);
            builder.Entity<Category>().HasMany<Post>(t => t.Posts).WithOne(t => t.Category).HasForeignKey(t => t.CategoryId);

            base.OnModelCreating(builder);

            foreach (var role in ApplicationDbInitializer.Roles)
            {
                builder.Entity<IdentityRole>().HasData(new IdentityRole { Name = role, NormalizedName = role.ToUpper() });

            }
        }
    }
    public static class ApplicationDbInitializer
    {
        public static string[] Roles = new string[] { "Admin", "Mod", "User" };

        public static void SeedUsers(UserManager<IdentityUser> userManager)
        {
            string psd = "MyPSW@007", userName = "abc@xyz.com", email = "abc@xyz.com";

            if (userManager.FindByEmailAsync(email).Result == null)
            {
                IdentityUser user = new IdentityUser
                {
                    UserName = userName,
                    Email = email
                };
                IdentityResult result = userManager.CreateAsync(user, psd).Result;

                if (result.Succeeded)
                {
                    foreach(string role in Roles)
                    {
                        userManager.AddToRoleAsync(user, role).Wait();
                    }
                }
            }
        }



    }
}
